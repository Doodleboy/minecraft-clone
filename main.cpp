#include <OpenGL/gl.h>
#include <GLUT/glut.h>
#include <iostream>
#include <stdio.h>
#include "block.cpp"
#include "player.cpp"
bool init = 0;
void display(){
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glColor3f(1.0,0.0,0.0);
  glBegin(GL_QUADS);
    glColor3f(0.0,1.0,0.0);
    glVertex3f(0.5,0.5,0.0);
    glVertex3f(0.5,0.0,0.0);
    glVertex3f(0.0,0.5,0.0);
    glVertex3f(0.0,0.0,0.0);
    glVertex3f(0.0,0.0,0.0);
  glEnd();
  glutSwapBuffers();
}
int main(int argc, char *argv[]){
  glutInit(&argc, argv);
  
  //Window
  glutInitWindowPosition(50, 50);
  glutInitWindowSize(640, 480);
  glutCreateWindow("Minecraft-Clone");
  
  //display
  glutDisplayFunc(display);
  //finishing main and handeling errors
  if (!init)
    return 1;
  glutMainLoop();
  return 0;
}
